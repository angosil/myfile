import exifread
import os
import shutil
from datetime import datetime


path_from = '/home/angosil/Downloads/image'
path_to = '/home/angosil/Pictures/'


def list_files(start_path):
    result_list_files = []
    for root, dirs, files in os.walk(start_path):
        for file_name in files:
            local_file = root + '/' + file_name
            result_list_files.append(
                {
                    'image_name': file_name,
                    'local_file': local_file,
                }
            )
    return result_list_files


def get_date_taken(path):
    # Open image file for reading (binary mode)
    image = open(path, 'rb')
    # Return Exif tags
    tags = exifread.process_file(image, details=False)
    if 'EXIF DateTimeOriginal' in tags:
        datetime_instance = datetime.strptime(str(tags['EXIF DateTimeOriginal']), '%Y:%m:%d  %H:%M:%S')
        file_name = datetime_instance.strftime('%Y:%m:%d')
        return file_name
    else:
        return "temp"


def copy_file():
    new_folder = get_date_taken(img_path_details['local_file'])
    destiny_folder = path_to + str(new_folder)
    if not os.path.exists(destiny_folder):
        os.mkdir(destiny_folder)
    destiny_file = destiny_folder + "/" + img_path_details['image_name']
    print(destiny_file)
    shutil.copy(img_path_details['local_file'], destiny_file)


files_list = list_files(path_from)
for img_path_details in files_list:
    copy_file()
